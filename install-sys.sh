#!bin/bash
# The Building of The Alvarium Cerium * The h1v3 n0d3 *

# Update/Upgrade system / install all dependencies

sudo apt-get update
sudo apt-get upgrade -y

sudo apt-get install apt-transport-https ca-certificates wget software-properties-common build-essential dialog make fldigi libcpanplus-perl libpackage-constants-perl libhttp-server-simple-perl libcrypt-cbc-perl libcrypt-rijndael-perl librpc-xml-perl libxml-feedpp-perl liblwp-protocol-socks-perl libnet-twitter-lite-perl libnet-server-perl libapache2-mod-php5 php5 php-pear php5-curl php5-gd apache2 apache2-doc mysql-server php5-mysql vim-common connect-proxy tor torsocks tor-geoipdb libavahi-client3 resolvconf avahi-autoipd avahi-dnsconfd liblockfile-bin liblockfile1 libxt6 lockfile-progs monkeysphere avahi-utils python-pip git xz-utils nodejs -y

# install docker / parrotsec and tools

adduser pirate

wget https://download.docker.com/linux/debian/gpg 
sudo apt-key add gpg
echo "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee -a /etc/apt/sources.list.d/docker.list
sudo apt-get update
sudo apt-get -y install docker-ce
sudo systemctl enable docker
sudo systemctl start docker
#allow nonroot
sudo groupadd docker
sudo useradd pirate
sudo usermod -aG docker pirate

## SECURITY OS RUNNING W OPENVAS, SNORT KIT w DEFENSE
# install parrotsec docker
docker pull parrotsec/parrot
docker run -ti --rm --network host parrotsec/parrot

docker pull parrotsec/metasploit
docker run -ti --network host parrotsec/metasploit

# more to do regarding setup and autoboot IDS and Defenses

# Injecting SEED
git clone https://******.***/*****/*****.git

cd Alvarium/Cerium/CORE/

sh init-core.sh